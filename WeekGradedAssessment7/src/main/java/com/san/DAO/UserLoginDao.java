package com.san.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.san.DBconnection.DbConnection;
import com.san.hcl.UserLogin;

public class UserLoginDao {
	public int storeNewAccount(UserLogin login) {
		try {
			Connection con = DbConnection.getDbConnection();
			PreparedStatement pstmt= con.prepareStatement("insert into login value(?,?,?,?)");
			
			pstmt.setLong(1, login.getMobileNumber());
			pstmt.setString(2, login.getEmail());
			pstmt.setString(3, login.getUserName());
			pstmt.setString(4, login.getPassword());
 
			return pstmt.executeUpdate();
		}catch(Exception e) {
			return 0;
		}
	}
	public boolean verifyPassword(Long mobileNumber, String password) {
		try {
			Connection con = DbConnection.getDbConnection();
			PreparedStatement pstmt= con.prepareStatement("select password from login where mobilenumber = ?");
		
			pstmt.setLong(1,mobileNumber);
			ResultSet rst= pstmt.executeQuery();
			
			if(rst.next()) {
				String password1 = rst.getString(1);
				if (password1.equals(password1)) {
					return true;
					}
				}
			}catch (Exception e) {
			
		}
		return false;
		
	}


}
